package info.scce.cinco.product.chess.action

import de.jabc.cinco.meta.runtime.action.CincoCustomAction
import info.scce.cinco.product.chess.chess.Board
import info.scce.cinco.product.chess.util.ChessExtension

class BoardPrepareAction extends CincoCustomAction<Board> {
	
	extension ChessExtension = ChessExtension.instance
	
	override getName() {
		"Prepare board"
	}
	
	override hasDoneChanges() {
		true
	}
	
    override canExecute(Board it) {
    	is8x8
    }
    
    override execute(Board it) {
    	prepare
    }
	
}
