package info.scce.cinco.product.chess.util

import info.scce.cinco.product.chess.chess.BlackBishop
import info.scce.cinco.product.chess.chess.BlackKing
import info.scce.cinco.product.chess.chess.BlackKnight
import info.scce.cinco.product.chess.chess.BlackPawn
import info.scce.cinco.product.chess.chess.BlackQueen
import info.scce.cinco.product.chess.chess.BlackRook
import info.scce.cinco.product.chess.chess.BlackSquare
import info.scce.cinco.product.chess.chess.Board
import info.scce.cinco.product.chess.chess.HorizontalLabel
import info.scce.cinco.product.chess.chess.Piece
import info.scce.cinco.product.chess.chess.Square
import info.scce.cinco.product.chess.chess.Table
import info.scce.cinco.product.chess.chess.VerticalLabel
import info.scce.cinco.product.chess.chess.WhiteBishop
import info.scce.cinco.product.chess.chess.WhiteKing
import info.scce.cinco.product.chess.chess.WhiteKnight
import info.scce.cinco.product.chess.chess.WhitePawn
import info.scce.cinco.product.chess.chess.WhiteQueen
import info.scce.cinco.product.chess.chess.WhiteRook
import info.scce.cinco.product.chess.chess.WhiteSquare

import static info.scce.cinco.product.chess.util.Color.*

class ChessExtension {
	
	
	
	// Static values
	
	val public static int DEFAULT_BORDER_SIZE = 40
	val public static int DEFAULT_SQUARE_SIZE = 80
	val public static int DEFAULT_PIECE_SIZE  = 80
	
	
	
	// Singleton pattern
	
	var static ChessExtension instance
	
	private new () {
		
	}
	
	def static ChessExtension getInstance() {
		if (instance === null) {
			instance = new ChessExtension
		}
		return instance
	}
	
	
	
	// Board
	
	def Table getTable(Board it) {
		if (container instanceof Table) {
			return container as Table
		}
	}
	
	def int getBoardWidth(Board it) {
		horizontalSquareCount * DEFAULT_SQUARE_SIZE + 2 * DEFAULT_BORDER_SIZE
	}
	
	def int getBoardHeight(Board it) {
		verticalSquareCount * DEFAULT_SQUARE_SIZE + 2 * DEFAULT_BORDER_SIZE
	}
	
	def boolean is8x8(Board it) {
		horizontalSquareCount == 8 && verticalSquareCount == 8
	}
	
	def String getHorizontalCoordinateName(Board it, int x) {
		val char a = 'A'
		val letter = (x + a) as char
		return letter.toString
	}
	
	def String getVerticalCoordinateName(Board it, int y) {
		return Integer.toString(verticalSquareCount - y)
	}
	
	def String getCoordinateName(Board it, int x, int y) {
		'''«getHorizontalCoordinateName(x)»«getVerticalCoordinateName(y)»'''
	}
	
	def Color getColor(int x, int y) {
		if ((x + y) % 2 == 0) BLACK else WHITE
	}
	
	def HorizontalLabel getHorizontalLabel(Board it, int x) {
		horizontalLabels.findFirst[position == x]
	}
	
	def VerticalLabel getVerticalLabel(Board it, int y) {
		verticalLabels.findFirst[position == y]
	}
	
	def Square getSquare(Board it, int x, int y) {
		squares.findFirst [
			horizontalPosition == x && verticalPosition == y
		]
	}
	
	def Square getOrNewSquare(Board it, int x, int y) {
		var square = getSquare(x, y)
		if (square === null) {
			val xPos = DEFAULT_BORDER_SIZE + x * DEFAULT_SQUARE_SIZE
			val yPos = DEFAULT_BORDER_SIZE + y * DEFAULT_SQUARE_SIZE
			square = switch getColor(x, y) {
				case BLACK: newWhiteSquare(xPos, yPos)
				case WHITE: newBlackSquare(xPos, yPos)
			}
			square.horizontalPosition = x
			square.verticalPosition   = y
			square.name = getCoordinateName(x, y)
		}
		return square
	}
	
	def Piece getPiece(Board it, int x, int y) {
		getSquare(x, y).piece
	}
	
	def Piece getOrNewPiece(Board it, int x, int y) {
		getSquare(x, y).getOrNewPiece
	}
	
	
	
	// Square
	
	def Board getBoard(Square it) {
		if (container instanceof Board) {
			return container as Board
		}
	}
	
	def Table getTable(Square it) {
		board.table
	}
	
	def Color getColor(Square it) {
		switch it {
			BlackSquare: BLACK
			WhiteSquare: WHITE
		}
	}
	
	def Piece getPiece(Square it) {
		pieces.head
	}
	
	def Piece getOrNewPiece(Square it) {
		var piece = piece
		if (piece === null && container.is8x8) {
			piece = if (verticalPosition == 0) {
				switch horizontalPosition {
					case 0, case 7: newBlackRook(0, 0)
					case 1, case 6: newBlackKnight(0, 0)
					case 2, case 5: newBlackBishop(0, 0)
					case 3:         newBlackQueen(0, 0)
					case 4:         newBlackKing(0, 0)
				}
			}
			else if (verticalPosition == 1) {
				newBlackPawn(0, 0)
			}
			else if (verticalPosition == 6) {
				newWhitePawn(0, 0)
			}
			else if (verticalPosition == 7) {
				switch horizontalPosition {
					case 0, case 7: newWhiteRook(0, 0)
					case 1, case 6: newWhiteKnight(0, 0)
					case 2, case 5: newWhiteBishop(0, 0)
					case 3:         newWhiteQueen(0, 0)
					case 4:         newWhiteKing(0, 0)
				}
			}
			piece.center
		}
		return piece
	}
	
	
	
	// Piece
	
	def Square getSquare(Piece it) {
		if (container instanceof Square) {
			return container as Square
		}
	}
	
	def Board getBoard(Piece it) {
		switch container: container {
			Board:  container
			Square: container.board
		}
	}
	
	def Table getTable(Piece it) {
		square.table
	}
	
	def Color getColor(Piece it) {
		switch it {
			BlackPawn,
			BlackRook,
			BlackKnight,
			BlackBishop,
			BlackQueen,
			BlackKing: BLACK
			WhitePawn,
			WhiteRook,
			WhiteKnight,
			WhiteBishop,
			WhiteQueen,
			WhiteKing: WHITE
		}
	}
	
	
	
	// Actions
	
	def void center(Piece it) {
		if (it === null) {
			return
		}
		resize(DEFAULT_PIECE_SIZE, DEFAULT_PIECE_SIZE)
		val position = (square.width - width) / 2
		move(position, position)
	}
	
	def void initialize(Board it) {
		// Clear Board
		labels.forEach[delete]
		squares.forEach[delete]
		// Resize Board
		resize(boardWidth, boardHeight)
		// Add Labels
		val topYPos    = 0
		val bottomYPos = DEFAULT_BORDER_SIZE + verticalSquareCount * DEFAULT_SQUARE_SIZE
		for (x: 0 ..< horizontalSquareCount) {
			val xPos = DEFAULT_BORDER_SIZE + x * DEFAULT_SQUARE_SIZE
			val topLabel    = newHorizontalLabel(xPos, topYPos)
			val bottomLabel = newHorizontalLabel(xPos, bottomYPos)
			val name = getHorizontalCoordinateName(x)
			topLabel.name    = name
			bottomLabel.name = name			
		}
		val leftXPos  = 0
		val rightXPos = DEFAULT_BORDER_SIZE + horizontalSquareCount * DEFAULT_SQUARE_SIZE
		for (y: 0 ..< verticalSquareCount) {
			val yPos = DEFAULT_BORDER_SIZE + y * DEFAULT_SQUARE_SIZE
			val name = getVerticalCoordinateName(y)
			newVerticalLabel(leftXPos,  yPos).name = name
			newVerticalLabel(rightXPos, yPos).name = name
		}
		// Add Squares
		for (y: 0 ..< verticalSquareCount) {
			for (x: 0 ..< horizontalSquareCount) {
				getOrNewSquare(x, y)
			}
		}
	}
	
	def void prepare(Board it) {
		for (square: squares) {
			square.pieces.forEach[delete]
			square.getOrNewPiece
		}
	}
	
	def void resetSize(Board it) {
		resize(boardWidth, boardHeight)
	}
	
}
